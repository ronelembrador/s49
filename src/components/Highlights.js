import {Row, Col, Card} from 'react-bootstrap'

export default function Highlight() {
	return(
		<Row className = "mt-3 mb-3">
			<Col xs = {12} md = {4}>
				<Card className = "cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Learn from Home</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione ipsum ea recusandae quidem perspiciatis voluptatem quia blanditiis magnam fugiat nisi aut, architecto commodi magni consequatur hic quisquam odio debitis odit!
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs = {12} md = {4}>
				<Card className = "cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing, elit. Obcaecati cum accusantium totam cumque nesciunt eveniet suscipit corporis temporibus ipsum, dolor laudantium veniam sunt molestiae soluta laborum blanditiis. Dicta ad, molestiae?
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs = {12} md = {4}>
				<Card className = "cardHighlight p-3">
					<Card.Body>
						<Card.Title>
							<h2>Be Part of our Community</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Cupiditate, libero. Autem optio similique repellat hic eum porro, minima ipsa sint excepturi, ipsam, eaque qui magnam expedita, natus quam odit reprehenderit?
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>

	)
}