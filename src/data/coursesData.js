const coursesData = [
	{
		id: 'wdc001',
		name: "PHP- Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt, numquam. Natus tempore nisi, dolorem eos itaque consequatur. Ut qui, vel facere, quisquam fuga asperiores eos mollitia ipsum, tempore beatae, inventore.",
		price: 45000,
		onOffer: true
	},
	{
		id: 'wdc002',
		name: "Python Jango",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt, numquam. Natus tempore nisi, dolorem eos itaque consequatur. Ut qui, vel facere, quisquam fuga asperiores eos mollitia ipsum, tempore beatae, inventore.",
		price: 55000,
		onOffer: true
	},
	{
		id: 'wdc003',
		name: "Java- Springboot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt, numquam. Natus tempore nisi, dolorem eos itaque consequatur. Ut qui, vel facere, quisquam fuga asperiores eos mollitia ipsum, tempore beatae, inventore.",
		price: 60000,
		onOffer: true
	}


]

export default coursesData;